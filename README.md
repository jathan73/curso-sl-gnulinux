Este curso esta diseñado para que las y los participantes conozcan la existencia del Software Libre, su
historia, principios, características técnicas y ventajas de uso, así como adquieran los conocimientos
necesarios para utilizar el sistema operativo GNU/Linux de manera predeterminada en una
computadora a través de la exploración y la utilización de la distribución Debian GNU/Linux,
enfocándonos a un uso que cubra las necesidades personales así como la realización de actividades de
trabajo dentro y fuera de la Informática.
